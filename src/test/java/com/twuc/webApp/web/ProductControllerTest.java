package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class ProductControllerTest extends IntegrationTestBase {

    @Autowired
    private ProductRepository repository;

    @Test
    void should_return_201_and_location() throws Exception {
        getMockMvc().perform(
            createProduct(new CreateProductRequest("Coca Cola", 3, "Bottle")))
            .andExpect(status().is(201))
            .andExpect(header().string("location", "http://localhost/api/products/1"));
    }

    @Test
    void should_return_400_if_request_is_invalid() throws Exception {
        getMockMvc()
            .perform(createProduct(new CreateProductRequest(null, 3, "Bottle")))
            .andExpect(status().is(400));
    }

    @Test
    void should_create_product() throws Exception {
        final CreateProductRequest expectedProduct = new CreateProductRequest("Coca Cola", 3, "Bottle");
        getMockMvc().perform(
            createProduct(expectedProduct))
            .andExpect(status().is(201));

        final Product product = repository.findById(1L).orElseThrow(RuntimeException::new);
        assertEquals(expectedProduct.getName(), product.getName());
        assertEquals(expectedProduct.getPrice(), product.getPrice());
        assertEquals(expectedProduct.getUnit(), product.getUnit());
    }

    @Test
    void should_get_product() throws Exception {
        final CreateProductRequest expectedProduct = new CreateProductRequest("Coca Cola", 3, "Bottle");
        final MockHttpServletResponse response = getMockMvc().perform(
            createProduct(expectedProduct))
            .andExpect(status().is(201))
            .andReturn().getResponse();
        final String location = response.getHeader("location");

        assertNotNull(location);

        getMockMvc().perform(get(location))
            .andExpect(status().is(200))
            .andExpect(jsonPath("$.name").value(is(expectedProduct.getName())))
            .andExpect(jsonPath("$.price").value(is(expectedProduct.getPrice())))
            .andExpect(jsonPath("$.unit").value(is(expectedProduct.getUnit())));
    }

    @Test
    void should_get_404_if_product_does_not_exist() throws Exception {
        final String notExistedProductUri = "/api/products/2333";
        getMockMvc().perform(get(notExistedProductUri))
            .andExpect(status().is(404));
    }

    private MockHttpServletRequestBuilder createProduct(CreateProductRequest product) throws Exception {
        return post("/api/products")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(
                new ObjectMapper()
                    .writeValueAsString(product));
    }
}