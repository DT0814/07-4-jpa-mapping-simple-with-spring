package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

/**
 * @author tao.dong
 */
@RestController
@RequestMapping("/api/products")
public class ProductController {
    @Autowired
    private ProductService service;

    @PostMapping
    public ResponseEntity<Void> create(@RequestBody @Valid CreateProductRequest productRequest) {
        Product product = service.createProduct(productRequest);
        return ResponseEntity.status(HttpStatus.CREATED)
                .header("location", "http://localhost/api/products/" + product.getId())
                .build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> get(@PathVariable Long id) {
        ResponseEntity responseEntity = null;

        Optional<Product> result = service.getProductById(id);
        if (result.isPresent()){
            responseEntity = ResponseEntity.ok().body(result.get());
        }else {
            responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        return responseEntity;
    }
}
