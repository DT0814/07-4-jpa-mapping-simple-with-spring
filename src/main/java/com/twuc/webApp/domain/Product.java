package com.twuc.webApp.domain;

import org.hibernate.validator.constraints.Length;
import sun.rmi.runtime.Log;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author tao.dong
 */
@Entity
public class Product {
    @Id
    @GeneratedValue
    private Long id;
    @Column()
    private String name;
    @Column()
    private Integer price;
    @Column()
    private String unit;

    public Product(String name, Integer price, String unit) {
        this.name = name;
        this.price = price;
        this.unit = unit;
    }

    public Product() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }
}
