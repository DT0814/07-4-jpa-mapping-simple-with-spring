package com.twuc.webApp.service;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author tao.dong
 */
@Service
public class ProductService {
    @Autowired
    private ProductRepository repository;

    public Product createProduct(CreateProductRequest productRequest) {
        Product product = new Product(productRequest.getName(),productRequest.getPrice(),productRequest.getUnit());
        return repository.saveAndFlush(product);
    }

    public Optional<Product> getProductById(Long id) {
        return repository.findById(id);
    }
}
